FROM python:3.7-alpine

WORKDIR /usr/src/app

COPY ./Pipfile* /usr/src/app/
RUN python3 -m pip install pipenv
RUN pipenv install --ignore-pipfile

COPY ./src/ /usr/src/app/

EXPOSE 5000
CMD ["pipenv", "run", "python3", "app.py"]
