#!/usr/bin/env python3

from flask import Flask, render_template, request, jsonify
import json

app = Flask(__name__)

WORDS = []

with open("words_dictionary.json", "r") as word_list:
    word_list = json.load(word_list)
    for word in word_list:
        WORDS.append(word)

@app.route("/search", methods = ['POST'])
def search():
    words = []
    searchword = request.args.get("q").lower()
    for word in WORDS:
        if searchword and word.startswith(searchword):
            words.append(word)
    return jsonify(words)

if __name__ == "__main__":
    app.run(debug=True, use_reloader=True, host='0.0.0.0', port=5000)
